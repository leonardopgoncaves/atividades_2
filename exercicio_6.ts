/*
Crie um algoritmo que solicite três números do usuário e exiba o maior deles.
*/

namespace exercicio_6
{
    let numero1, numero2, numero3: number;
    numero1 = 1;
    numero2 = 4;
    numero3 = 2;

    if(numero3 > numero2 && numero3 > numero1)
    {
        console.log(`Este número é o maior: ${numero3}`);
    }

    else if (numero2 > numero3 && numero2 > numero1)
    {
        console.log(`Este número é o maior: ${numero2}`);
    }

    else 
    {
        console.log(`Este número é o maior: ${numero1}`);
    }
}