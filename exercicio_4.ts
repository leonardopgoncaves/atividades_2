/*
Faça um programa que recebe um número inteiro e verifique se esse número é par ou ímpar.
*/

namespace exercicio_4
{
    let numero, resultado: number;

    numero = 2;

    resultado = numero % 2;

    if (resultado == 0)
    {
        console.log("par");
    } 
    else
    {
        console.log("impar");
    }
}