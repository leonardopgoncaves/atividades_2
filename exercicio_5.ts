/*
Crie um algoritmo que solicite o nome de um usuário e exiba uma mensagem de boas-vindas personalizada de acordo com o horário do dia (bom dia, boa tarde ou boa noite).
*/

namespace exercicio_5
{
    let usuario: string;
    usuario = "Bruno primo do vinicius"

    const data = new Date();
    const hora = data.getHours();

    if (hora >= 6 && hora < 12)
    {
        console.log(`Bom dia ${usuario}`);
    }

    else if (hora >= 12 && hora < 18)
    {
        console.log(`Boa tarde ${usuario}`);
    }

    else 
    {
        console.log(`Boa noite ${usuario}`);
    }
}