/*
A nota final de um estudante é calculada a partir de três notas atribuídas respectivamente a um trabalho de laboratório, a uma avaliação semestral e a um exame final. A média das três notas mencionadas anteriormente obedece aos pesos a seguir.
*/

namespace exercicio_2
{
    let nota1, nota2, nota3, resultado: number;

    nota1 = 1;
    nota2 = 1;
    nota3 = 1;

    resultado = (nota1 * 2 + nota2 * 3 + nota3 * 5) / (2 + 3 + 5);

    console.log(resultado);

    if (resultado >= 8)
    {
        console.log("conceito A");
    }

    if (resultado >= 7 && resultado < 8)
    {
        console.log("conceito B")
    }

    if (resultado >= 6 && resultado < 7)
    {
        console.log("conceito C");
    }

    if (resultado >= 5 && resultado < 6)
    {
        console.log("conceito D");
    }

    if (resultado < 5)
    {
        console.log("conceito E");
    }
}