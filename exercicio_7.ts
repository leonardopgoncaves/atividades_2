/*
Faça um programa que leia um número inteiro entre 1 e 12 e imprima o mês correspondente, sendo 1 para janeiro, 2 para fevereiro, e assim por diante.
*/

namespace exercicio_7
{
    let numero: number = 1;

    if (numero == 1)
    {
        console.log("Janeiro");
    }

    else if (numero == 2)
    {
        console.log("Fevereiro")
    }

    else if (numero == 3)
    {
        console.log("Março")
    }

    else if (numero == 4)
    {
        console.log("Abril")
    }

    else if (numero == 5)
    {
        console.log("Maio")
    }

    else if (numero == 6)
    {
        console.log("Junho")
    }

    else if (numero == 7)
    {
        console.log("Julho")
    }

    else if (numero == 8)
    {
        console.log("Agosto")
    }

    else if (numero == 9)
    {
        console.log("Setembro")
    }

    else if (numero == 10)
    {
        console.log("Outubro")
    }

    else if (numero == 11)
    {
        console.log("Novembro")
    }

    else
    {
        console.log("Dezembro")
    }
}