namespace exemplos_condicoes
{
    //condição
    let idade: number = 10;

    if(idade >= 18)
    {
        console.log("Pode dirigir");
    }
    else
    {
        console.log("Não pode dirigir");
    }

    //terminário
    idade >= 18 ? console.log("Pode dirigir") : console.log("Não pode dirigir");
}