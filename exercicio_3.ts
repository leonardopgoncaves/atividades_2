/*
Faça um programa que receba três números obrigatoriamente em ordem crescente e um quarto número que não siga esta regra. Mostre, em seguida, os quatro números em ordem decrescente.
*/

namespace exercicio_3
{
    let numero1, numero2, numero3, numero4: number;

    numero1 = 1;
    numero2 = 4;
    numero3 = 9;
    numero4 = 10;

    if (numero4 > numero3)
    {
        console.log(`${numero4} - ${numero3} - ${numero2} - ${numero1}`);
    }

    else if (numero4 > numero2)
    {
        console.log(`${numero3} - ${numero4} - ${numero2} - ${numero1}`);
    }

    else if (numero4 > numero1)
    {
        console.log(`${numero3} - ${numero2} - ${numero4} - ${numero1}`);
    }

    else
    {
        console.log(`${numero3} - ${numero2} - ${numero1} - ${numero4}`);
    }
}